<?xml version="1.0" encoding="utf-8"?>

<layout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>
        <variable
            name="movie"
            type="in.example.moviedemo.network.MovieDetailResponse" />
    </data>

    <androidx.coordinatorlayout.widget.CoordinatorLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        tools:context=".activity.MovieDetailActivity">

        <com.google.android.material.appbar.AppBarLayout
            android:id="@+id/movie_detail_ab"
            android:layout_width="match_parent"
            android:layout_height="@dimen/_200sdp"
            android:fitsSystemWindows="true"
            android:theme="@style/Theme.MovieDemo">

            <com.google.android.material.appbar.CollapsingToolbarLayout
                android:id="@+id/movie_detail_ctl"
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:fitsSystemWindows="true"
                app:contentScrim="?attr/colorPrimary"
                app:expandedTitleTextAppearance="@android:color/transparent"
                app:layout_scrollFlags="scroll|exitUntilCollapsed">

                <com.google.android.material.imageview.ShapeableImageView
                    android:id="@+id/movie_detail_backdrop_iv"
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:scaleType="centerCrop"
                    app:imageUrl="@{movie.backdropPath}"/>

                <com.google.android.material.appbar.MaterialToolbar
                    android:id="@+id/movie_detail_toolbar"
                    android:layout_width="match_parent"
                    android:layout_height="?attr/actionBarSize"
                    app:layout_collapseMode="pin">

                    <com.google.android.material.textview.MaterialTextView
                        android:id="@+id/movie_detail_toolbar_title_tv"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        tools:text="Movie Name"
                        android:text="@{movie.title}"
                        android:fontFamily="@font/nunito_sans_semi_bold"
                        android:textColor="@color/colorOnPrimary"
                        android:textAppearance="@style/TextAppearance.AppCompat.Medium" />

                </com.google.android.material.appbar.MaterialToolbar>

            </com.google.android.material.appbar.CollapsingToolbarLayout>

        </com.google.android.material.appbar.AppBarLayout>

        <androidx.core.widget.NestedScrollView
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            app:layout_behavior="com.google.android.material.appbar.AppBarLayout$ScrollingViewBehavior">

            <androidx.constraintlayout.widget.ConstraintLayout
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:orientation="vertical"
                android:layout_marginTop="@dimen/_12sdp"
                android:padding="@dimen/_12sdp">

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/row_movie_name_tv"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    tools:text="Drishyam - Visuals Can Be Deceptive"
                    android:text="@{movie.title + movie.tagline}"
                    android:textAppearance="@style/TextAppearance.AppCompat.Large"
                    android:textColor="@color/colorOnPrimary"
                    android:fontFamily="@font/nunito_sans_semi_bold"/>

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/row_movie_duration_tv"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/row_movie_name_tv"
                    app:layout_constraintBaseline_toBaselineOf="@+id/row_movie_rate_tv"
                    tools:text="2 hr 43 mins"
                    app:duration="@{movie.runtime}"
                    android:textAppearance="@style/TextAppearance.AppCompat.Small"
                    android:textColor="@color/color_black_60"
                    android:layout_marginTop="@dimen/_8sdp"
                    app:layout_constraintHorizontal_bias="0"
                    android:fontFamily="@font/nunito_sans_regular"/>

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/row_movie_rate_tv"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    app:layout_constraintStart_toEndOf="@+id/row_movie_duration_tv"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/row_movie_name_tv"
                    tools:text="8.1"
                    android:text="@{Double.toString(movie.voteAverage)}"
                    android:textAppearance="@style/TextAppearance.AppCompat.Small"
                    android:textColor="@color/colorOnPrimary"
                    android:fontFamily="@font/nunito_sans_regular"
                    app:drawableStartCompat="@drawable/ic_star_rate"
                    android:drawablePadding="@dimen/_4sdp"
                    android:layout_marginTop="@dimen/_8sdp"
                    app:layout_constraintHorizontal_bias="1"
                    android:gravity="center_vertical"/>

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/row_movie_overview_tv"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/row_movie_rate_tv"
                    tools:text="A simple, street-smart man tries to protect his family from a tough cop looking for his missing son."
                    android:text="@{movie.overview}"
                    android:textAppearance="@style/TextAppearance.AppCompat.Medium"
                    android:textColor="@color/colorOnPrimary"
                    android:layout_marginTop="@dimen/_8sdp"
                    android:fontFamily="@font/nunito_sans_regular"/>

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/row_movie_date_tv"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/row_movie_overview_tv"
                    tools:text="Release Date: 05/12/2021"
                    app:date="@{movie.releaseDate}"
                    android:textAppearance="@style/TextAppearance.AppCompat.Small"
                    android:textColor="@color/color_black_60"
                    android:fontFamily="@font/nunito_sans_regular"
                    android:layout_marginTop="@dimen/_8sdp"/>

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/row_movie_vote_tv"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/row_movie_date_tv"
                    tools:text="Votes: 125"
                    android:text="@{`Votes: ` + movie.voteCount}"
                    android:textAppearance="@style/TextAppearance.AppCompat.Small"
                    android:textColor="@color/color_black_60"
                    android:fontFamily="@font/nunito_sans_regular"
                    android:layout_marginTop="@dimen/_8sdp"/>


            </androidx.constraintlayout.widget.ConstraintLayout>

        </androidx.core.widget.NestedScrollView>

        <com.google.android.material.floatingactionbutton.FloatingActionButton
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginEnd="@dimen/_30sdp"
            android:baselineAlignBottom="false"
            android:clickable="true"
            android:contentDescription="@string/app_name"
            android:focusable="true"
            android:src="@drawable/ic_add"
            app:layout_anchor="@id/movie_detail_ab"
            app:layout_anchorGravity="bottom|end" />

        <ProgressBar
            android:id="@+id/movie_detail_progress_bar"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_gravity="center"/>

    </androidx.coordinatorlayout.widget.CoordinatorLayout>

</layout>
