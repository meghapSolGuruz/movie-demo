package `in`.example.moviedemo.hilt

import `in`.example.moviedemo.database.MovieDao
import `in`.example.moviedemo.database.MovieDatabase
import `in`.example.moviedemo.network.BASE_URL
import `in`.example.moviedemo.network.RetrofitInterface
import `in`.ms.moviedemo.module.DatabaseManager
import `in`.ms.moviedemo.module.NetworkManager
import android.util.Log
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object MovieModule {

    @Provides
    @Singleton
    fun provideRetrofitInterface(): RetrofitInterface {

        val httpClient : OkHttpClient by lazy {
            OkHttpClient.Builder()
                .addInterceptor { chain ->
                    val original = chain.request()
                    val keyParameterUrl: HttpUrl =
                        original.url().newBuilder()
                            .addQueryParameter("api_key", "5a9bd9f34a0a518eff760bb96bc57027")
                            .addQueryParameter("language", "en-US")
                            .addQueryParameter("with_original_language", "en")
                            .build()
                    chain.proceed(original.newBuilder().url(keyParameterUrl).build())
                }
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build()
        }

        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(RetrofitInterface::class.java)
    }

    @Provides
    @Singleton
    fun provideDatabaseManager(movieDao: MovieDao): DatabaseManager {
        return DatabaseManager(movieDao)
    }

    @Provides
    @Singleton
    fun provideMovieDatabase(): MovieDatabase {
        return MovieDatabase.getInstance()
    }

    @Provides
    @Singleton
    fun provideMovieDao(movieDatabase: MovieDatabase): MovieDao {
        return movieDatabase.getMovieDao()
    }

    @Provides
    @Singleton
    fun provideNetworkManager(retrofitApi: RetrofitInterface): NetworkManager {
        Log.e("Module", "provideNetworkManager invoked")
        return NetworkManager(retrofitApi)
    }

    /*@Provides
    @Singleton
    fun provideMovieRepository(databaseManager: DatabaseManager, networkManager: NetworkManager): MovieRepository {
        return MovieRepository(databaseManager, networkManager)
    }*/

}
