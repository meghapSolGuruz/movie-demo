package `in`.example.moviedemo.database

import `in`.example.moviedemo.model.Movie
import androidx.room.*

@Dao
interface MovieDao {

    @Query("SELECT * FROM movie WHERE category = :category LIMIT :page OFFSET :offset")
    fun getMovie(category: String, page: Int, offset: Int) : List<Movie>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovie(movie: List<Movie>)

    @Update
    fun updateMovie(movie: Movie)

    @Delete
    fun deleteMovie(movie: Movie)
}