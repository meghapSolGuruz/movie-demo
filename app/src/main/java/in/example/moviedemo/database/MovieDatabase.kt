package `in`.example.moviedemo.database

import `in`.example.moviedemo.App
import `in`.example.moviedemo.model.Movie
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Movie::class], exportSchema = false, version = 1)
abstract class MovieDatabase() : RoomDatabase() {

    abstract fun getMovieDao() : MovieDao

    companion object {
        private var INSTANCE: MovieDatabase? = null
        fun getInstance() : MovieDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(App.getAppContext(), MovieDatabase::class.java, "movie_database")
                    .allowMainThreadQueries()
                    .build()
                INSTANCE = instance
                instance
            }
        }
    }
}