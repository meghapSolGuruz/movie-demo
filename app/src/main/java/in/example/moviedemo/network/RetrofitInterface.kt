package `in`.example.moviedemo.network

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

const val BASE_URL: String = "https://api.themoviedb.org/3/"

interface RetrofitInterface {

    @GET("movie/popular")
    suspend fun getPopularMovies(@Query("page") page: Int) : Response<MovieResponse>

    @GET("movie/top_rated")
    suspend fun getTopRatedMovies(@Query("page") page: Int) : Response<MovieResponse>

    @GET("movie/upcoming")
    suspend fun getUpcomingMovies(@Query("page") page: Int) : Response<MovieResponse>

    @GET("movie/{movie_id}")
    suspend fun getMovieDetail(
            @Path("movie_id") movie_id: Int
    ): Response<MovieDetailResponse>
}