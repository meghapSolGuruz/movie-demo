package `in`.example.moviedemo.network

import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {

    private const val BASE_URL: String = "https://api.themoviedb.org/3/"

    private val httpClient : OkHttpClient by lazy {
        OkHttpClient.Builder()
            .addInterceptor { chain ->
                val original = chain.request()
                val keyParameterUrl: HttpUrl =
                    original.url().newBuilder()
                        .addQueryParameter("api_key", "5a9bd9f34a0a518eff760bb96bc57027")
                        .addQueryParameter("language", "en-US")
                        .addQueryParameter("with_original_language", "en")
                        .build()
                chain.proceed(original.newBuilder().url(keyParameterUrl).build())
            }
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()
    }

    private val retrofit : Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val retrofitAPi : RetrofitInterface by lazy {
        retrofit.create(RetrofitInterface::class.java)
    }
}