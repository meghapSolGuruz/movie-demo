package `in`.example.moviedemo.network

interface ResponseCallback<T> {
    fun onResponse(movieResponse: T)
}