package `in`.example.moviedemo.network

import `in`.example.moviedemo.utils.ResponseStatus
import `in`.example.moviedemo.model.Movie
import com.google.gson.annotations.SerializedName

data class MovieResponse(var result: ResponseStatus, var message: String,
        @SerializedName("page")
        val page: Int,
        @SerializedName("results")
        val movies: ArrayList<Movie>?,
        var extra: Any?
)