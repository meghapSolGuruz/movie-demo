package `in`.example.moviedemo.network

import `in`.example.moviedemo.model.Genre
import `in`.example.moviedemo.utils.ResponseStatus
import com.google.gson.annotations.SerializedName

data class MovieDetailResponse(
    var result: ResponseStatus,
    var message: String,
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("backdrop_path")
    val backdropPath: String? = null,
    @SerializedName("genres")
    val genres: ArrayList<Genre>? = null,
    @SerializedName("original_title")
    val originalTitle: String? = null,
    @SerializedName("overview")
    val overview: String? = null,
    @SerializedName("poster_path")
    val posterPath: String? = null,
    @SerializedName("release_date")
    val releaseDate: String? = null,
    @SerializedName("tagline")
    val tagline: String? = null,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("vote_average")
    val voteAverage: Double? = null,
    @SerializedName("vote_count")
    val voteCount: Int? = null,
    @SerializedName("runtime")
    val runtime: Int? = null,
)