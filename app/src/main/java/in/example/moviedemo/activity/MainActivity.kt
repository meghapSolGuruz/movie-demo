package `in`.example.moviedemo.activity

import `in`.example.moviedemo.R
import `in`.example.moviedemo.adapter.MovieTabFragmentAdapter
import `in`.example.moviedemo.databinding.ActivityMainBinding
import `in`.example.moviedemo.fragment.MovieFragment
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View.VISIBLE
import androidx.databinding.DataBindingUtil
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.delay

//import kotlinx.android.synthetic.main.activity_main.*
//import kotlinx.android.synthetic.main.layout_toolbar.view.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setSupportActionBar(binding.mainToolbar.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        binding.mainToolbar.toolbarTitle.text = getString(R.string.string_movie_title)

        val fragmentList = arrayListOf(MovieFragment.getInstance("popular"), MovieFragment.getInstance("top_rated"), MovieFragment.getInstance("upcoming"))
        binding.mainViewPager.adapter = MovieTabFragmentAdapter(this, fragmentList)

        // Setup Tab view
        binding.mainToolbar.toolbarTabLayout.visibility = VISIBLE
        TabLayoutMediator(binding.mainToolbar.toolbarTabLayout, binding.mainViewPager) { tab, position ->
            tab.text = when (position) {
                0 -> "Popular"
                1 -> "Top Rated"
                else -> "Upcoming"
            }
        }.attach()
    }
}