package `in`.example.moviedemo.activity

import `in`.example.moviedemo.App
import `in`.example.moviedemo.R
import `in`.example.moviedemo.databinding.ActivityMovieDetailBinding
import `in`.example.moviedemo.utils.LoadingStatus
import `in`.example.moviedemo.viewmodel.MovieDetailViewModel
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMovieDetailBinding

    private var movieId: Int = 0

    private val viewModel: MovieDetailViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_movie_detail)

        setSupportActionBar(binding.movieDetailToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        val bundle= intent.extras
        movieId = bundle?.getLong("movie_id")?.toInt() ?: 0

        observeUI()

        viewModel.getMovieDetail(movieId)
    }

    private fun observeUI() {
        viewModel.movieDetail.observe(this) {
            it?.let {
                binding.movie = it
            } ?: Toast.makeText(App.getAppContext(), getString(R.string.string_something_went_wrong), Toast.LENGTH_LONG).show()
        }

        viewModel.loadingStatus.observe(this) {
            binding.movieDetailProgressBar.visibility = if (it == LoadingStatus.STATUS_LOADING) View.VISIBLE else View.GONE
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }
}