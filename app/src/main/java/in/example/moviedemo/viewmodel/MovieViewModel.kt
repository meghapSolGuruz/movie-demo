package `in`.example.moviedemo.viewmodel

import `in`.example.moviedemo.utils.LoadingStatus
import `in`.example.moviedemo.utils.NetworkUtils
import `in`.example.moviedemo.utils.ResponseStatus
import `in`.example.moviedemo.model.Movie
import `in`.example.moviedemo.network.MovieResponse
import `in`.example.moviedemo.network.ResponseCallback
import `in`.ms.moviedemo.module.DatabaseManager
import `in`.ms.moviedemo.module.NetworkManager
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MovieViewModel @Inject constructor(private val networkManager: NetworkManager, private val databaseManager: DatabaseManager) : ViewModel() {

    val loadingStatus: MutableLiveData<LoadingStatus> = MutableLiveData()

    // Popular
    val popularMovieList: MutableLiveData<ArrayList<Movie>> = MutableLiveData()
    private var popularMoviePage: Int = 1
    var isPopularSourceOnline: Boolean = false

    // Top Rated
    val topRatedMovieList: MutableLiveData<ArrayList<Movie>> = MutableLiveData()
    private var topRatedMoviePage: Int = 1
    var isTopRatedSourceOnline: Boolean = false

    // Upcoming
    val upcomingMovieList: MutableLiveData<ArrayList<Movie>> = MutableLiveData()
    private var upcomingMoviePage: Int = 1
    var isUpcomingSourceOnline: Boolean = false

    /*private val networkManager: NetworkManager by lazy {
        NetworkManager()
    }

    private val databaseManager: DatabaseManager by lazy {
        DatabaseManager()
    }*/

    /**
     *  Popular
     */
    fun getPopularMovie() {
        isPopularSourceOnline = isPopularSourceOnline || (popularMoviePage == 1 && NetworkUtils.isInternetConnected())
        if (isPopularSourceOnline) {
            getOnlinePopularMovie()
        } else {
            getOfflinePopularMovie()
        }
    }

    private fun getOnlinePopularMovie() {
        loadingStatus.postValue(LoadingStatus.STATUS_LOADING)
        networkManager.getPopularMovie(popularMoviePage, popularResponseCallback)
    }

    private fun getOfflinePopularMovie() {
        databaseManager.getMovies("popular", popularMoviePage, popularResponseCallback)
    }

    private val popularResponseCallback = object : ResponseCallback<MovieResponse> {
        override fun onResponse(movieResponse: MovieResponse) {
            if (movieResponse.result == ResponseStatus.RESPONSE_SUCCESS) {
                popularMoviePage += 1
                movieResponse.movies?.let {
                    if (isPopularSourceOnline) {
                        val tmpMovies = movieResponse.movies
                        tmpMovies.map {
                            it.category = "popular"
                        }
                        databaseManager.insertMovie(tmpMovies)
                    }
                    popularMovieList.postValue(popularMovieList.value?.apply { addAll(movieResponse.movies) } ?: movieResponse.movies)
                    print(movieResponse.movies.toString())
                }
            }
            loadingStatus.postValue(LoadingStatus.STATUS_FINISH)
        }
    }


    /**
     *  Top Rated
     */
    fun getTopRatedMovie() {
        isTopRatedSourceOnline = isTopRatedSourceOnline || (topRatedMoviePage == 1 && NetworkUtils.isInternetConnected())
        if (isTopRatedSourceOnline) {
            getOnlineTopRatedMovie()
        } else {
            getOfflineTopRatedMovie()
        }
    }

    private fun getOnlineTopRatedMovie() {
        loadingStatus.postValue(LoadingStatus.STATUS_LOADING)
        networkManager.getTopRatedMovie(topRatedMoviePage, topRatedResponseCallback)
    }

    private fun getOfflineTopRatedMovie() {
        databaseManager.getMovies("top_rated", topRatedMoviePage, topRatedResponseCallback)
    }

    private val topRatedResponseCallback = object : ResponseCallback<MovieResponse> {
        override fun onResponse(movieResponse: MovieResponse) {
            if (movieResponse.result == ResponseStatus.RESPONSE_SUCCESS) {
                topRatedMoviePage += 1
                movieResponse.movies?.let {
                    if (isTopRatedSourceOnline) {
                        val tmpMovies = movieResponse.movies
                        tmpMovies.map {
                            it.category = "top_rated"
                        }
                        databaseManager.insertMovie(tmpMovies)
                    }
                    topRatedMovieList.postValue(topRatedMovieList.value?.apply { addAll(movieResponse.movies) } ?: movieResponse.movies)
                    print(movieResponse.movies.toString())
                }
            }
            loadingStatus.postValue(LoadingStatus.STATUS_FINISH)
        }
    }


    /**
     *  Upcoming
     */
    fun getUpcomingMovie() {
        isUpcomingSourceOnline = isUpcomingSourceOnline || (upcomingMoviePage == 1 && NetworkUtils.isInternetConnected())
        if (isUpcomingSourceOnline) {
            getOnlineUpcomingMovie()
        } else {
            getOfflineUpcomingMovie()
        }
    }

    private fun getOnlineUpcomingMovie() {
        loadingStatus.postValue(LoadingStatus.STATUS_LOADING)
        networkManager.getUpcomingMovie(upcomingMoviePage, upcomingResponseCallback)
    }

    private fun getOfflineUpcomingMovie() {
        databaseManager.getMovies("upcoming", upcomingMoviePage, upcomingResponseCallback)
    }

    private val upcomingResponseCallback = object : ResponseCallback<MovieResponse> {
        override fun onResponse(movieResponse: MovieResponse) {
            if (movieResponse.result == ResponseStatus.RESPONSE_SUCCESS) {
                upcomingMoviePage += 1
                movieResponse.movies?.let {
                    if (isUpcomingSourceOnline) {
                        val tmpMovies = movieResponse.movies
                        tmpMovies.map {
                            it.category = "upcoming"
                        }
                        databaseManager.insertMovie(tmpMovies)
                    }
                    upcomingMovieList.postValue(upcomingMovieList.value?.apply { addAll(movieResponse.movies) } ?: movieResponse.movies)
                    print(movieResponse.movies.toString())
                }
            }
            loadingStatus.postValue(LoadingStatus.STATUS_FINISH)
        }
    }
}