package `in`.example.moviedemo.viewmodel

import `in`.example.moviedemo.network.MovieDetailResponse
import `in`.example.moviedemo.network.ResponseCallback
import `in`.example.moviedemo.utils.LoadingStatus
import `in`.example.moviedemo.utils.ResponseStatus
import `in`.ms.moviedemo.module.NetworkManager
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel @Inject constructor(private val networkManager: NetworkManager): ViewModel() {

    val movieDetail: MutableLiveData<MovieDetailResponse> = MutableLiveData()

    val loadingStatus: MutableLiveData<LoadingStatus> = MutableLiveData()

    fun getMovieDetail(movieId: Int) {
        loadingStatus.postValue(LoadingStatus.STATUS_LOADING)
        networkManager.getMovieById(movieId, responseCallback)
    }

    private val responseCallback = object : ResponseCallback<MovieDetailResponse> {
        override fun onResponse(movieResponse: MovieDetailResponse) {
            if (movieResponse.result == ResponseStatus.RESPONSE_SUCCESS) {
                movieDetail.postValue(movieResponse)
            }
            loadingStatus.postValue(LoadingStatus.STATUS_FINISH)
        }
    }
}