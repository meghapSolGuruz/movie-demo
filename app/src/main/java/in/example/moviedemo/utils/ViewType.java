package in.example.moviedemo.utils;

public enum ViewType {
    TYPE_GRID, TYPE_LIST
}
