package `in`.example.moviedemo.utils

enum class LoadingStatus {
    STATUS_LOADING,
    STATUS_FINISH
}