package `in`.example.moviedemo.utils

enum class ResponseStatus {
    RESPONSE_SUCCESS, RESPONSE_ERROR, RESPONSE_FAILED
}