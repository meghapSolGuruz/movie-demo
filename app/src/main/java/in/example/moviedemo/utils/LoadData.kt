package `in`.example.moviedemo.utils

import `in`.example.moviedemo.App
import `in`.example.moviedemo.R
import android.text.TextUtils
import android.util.Log
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.google.android.material.imageview.ShapeableImageView
import com.google.android.material.textview.MaterialTextView
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class LoadData {

    companion object {
        @JvmStatic
        @BindingAdapter("imageUrl")
        fun loadImage(view: ShapeableImageView, url: String?) {
            if (!TextUtils.isEmpty(url))
                Glide.with(view.context).load("https://image.tmdb.org/t/p/original$url").placeholder(R.drawable.plceholder).into(view)
        }

        @JvmStatic
        @BindingAdapter("duration")
        fun getDuration(view: MaterialTextView, runtime: Int) {
            if (runtime != 0) {
                val hours: Int = runtime / 60
                val min: Int = runtime % 60
                view.text = if (hours == 0) "$min min" else if (min == 0) "$hours hr" else "$hours hr $min min"
            } else {
                view.text = "0 min"
            }
        }

        @JvmStatic
        @BindingAdapter("date")
        fun getDate(view: MaterialTextView, date: String?) {
//            Log.e("date", date)
            if (!TextUtils.isEmpty(date)) {
                val originalFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
                val targetFormat: DateFormat = SimpleDateFormat("dd MMM, yyyy", Locale.ENGLISH)
                view.text = String.format(App.getAppContext().getString(R.string.string_date_format), targetFormat.format(originalFormat.parse(date) as Date))
            } else {
                view.text = ""
            }
        }
    }
}