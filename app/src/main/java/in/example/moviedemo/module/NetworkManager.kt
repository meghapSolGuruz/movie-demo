package `in`.ms.moviedemo.module

import `in`.example.moviedemo.R
import `in`.example.moviedemo.utils.ResponseStatus
import `in`.example.moviedemo.network.*
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.IOException
import javax.inject.Inject

class NetworkManager @Inject constructor(private val retrofitApi: RetrofitInterface) {

    fun getPopularMovie(page: Int, responseCallback: ResponseCallback<MovieResponse>) {

        CoroutineScope(Dispatchers.IO).launch {
            val response = retrofitApi.getPopularMovies(page);
            if (response.isSuccessful) {
                responseCallback.onResponse(response.body()?.apply { result = ResponseStatus.RESPONSE_SUCCESS } ?: MovieResponse(ResponseStatus.RESPONSE_ERROR, "Something went wrong", page, null, null))
            } else {
                var movieResponse: MovieResponse? = null
                try {
                    val data = response.errorBody()?.string()
                    movieResponse = Gson().fromJson(data, MovieResponse::class.java)
                } catch (ioException: IOException) {
                    ioException.printStackTrace()
                }
                responseCallback.onResponse(movieResponse?.apply { result = ResponseStatus.RESPONSE_ERROR } ?: MovieResponse(ResponseStatus.RESPONSE_ERROR, "Something went wrong", page, null, null))
            }
        }
    }

    fun getTopRatedMovie(page: Int, responseCallback: ResponseCallback<MovieResponse>) {

        CoroutineScope(Dispatchers.IO).launch {
            val response = retrofitApi.getTopRatedMovies(page)
            if (response.isSuccessful) {
                responseCallback.onResponse(response.body()?.apply { result = ResponseStatus.RESPONSE_SUCCESS } ?: MovieResponse(ResponseStatus.RESPONSE_ERROR, "Something went wrong", page, null, null))
            } else {
                var movieResponse: MovieResponse? = null
                try {
                    val data = response.errorBody()?.string()
                    movieResponse = Gson().fromJson(data, MovieResponse::class.java)
                } catch (ioException: IOException) {
                    ioException.printStackTrace()
                }
                responseCallback.onResponse(movieResponse?.apply { result = ResponseStatus.RESPONSE_ERROR } ?: MovieResponse(ResponseStatus.RESPONSE_ERROR, "Something went wrong", page, null, null))
            }
        }
    }

    fun getUpcomingMovie(page: Int, responseCallback: ResponseCallback<MovieResponse>) {

        CoroutineScope(Dispatchers.IO).launch {
            val response = retrofitApi.getUpcomingMovies(page)
            if (response.isSuccessful) {
                responseCallback.onResponse(response.body()?.apply { result = ResponseStatus.RESPONSE_SUCCESS } ?: MovieResponse(ResponseStatus.RESPONSE_ERROR, "Something went wrong", page, null, null))
            } else {
                var movieResponse: MovieResponse? = null
                try {
                    val data = response.errorBody()?.string()
                    movieResponse = Gson().fromJson(data, MovieResponse::class.java)
                } catch (ioException: IOException) {
                    ioException.printStackTrace()
                }
                responseCallback.onResponse(movieResponse?.apply { result = ResponseStatus.RESPONSE_ERROR } ?: MovieResponse(ResponseStatus.RESPONSE_ERROR, "Something went wrong", page, null, null))
            }
        }
    }

    fun getMovieById(movieId: Int, responseCallback: ResponseCallback<MovieDetailResponse>) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = retrofitApi.getMovieDetail(movieId)
            if (response.isSuccessful) {
                responseCallback.onResponse(response.body()?.apply { result = ResponseStatus.RESPONSE_SUCCESS } ?: MovieDetailResponse(result = ResponseStatus.RESPONSE_ERROR, message = "${R.string.string_something_went_wrong}"))
            } else {
                var detailResponse: MovieDetailResponse? = null
                try {
                    val error = response.errorBody()?.string()
                    detailResponse = Gson().fromJson(error, MovieDetailResponse::class.java)
                } catch (ioException: IOException) {
                    ioException.printStackTrace()
                }
                responseCallback.onResponse(detailResponse?.apply { result = ResponseStatus.RESPONSE_ERROR } ?: MovieDetailResponse(result = ResponseStatus.RESPONSE_ERROR, message = "${R.string.string_something_went_wrong}"))
            }
        }
    }
}