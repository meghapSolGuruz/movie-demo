package `in`.ms.moviedemo.module

import `in`.example.moviedemo.database.MovieDao
import `in`.example.moviedemo.utils.ResponseStatus
import `in`.example.moviedemo.database.MovieDatabase
import `in`.example.moviedemo.model.Movie
import `in`.example.moviedemo.network.MovieResponse
import `in`.example.moviedemo.network.ResponseCallback
import android.nfc.tech.MifareUltralight.PAGE_SIZE
import javax.inject.Inject

class DatabaseManager @Inject constructor(private val movieDao: MovieDao) {

//    private val movieDao = MovieDatabase.getInstance().getMovieDao()

    fun getMovies(category: String, page: Int, responseCallback: ResponseCallback<MovieResponse>) {
        responseCallback.onResponse(getMovie(page, category))
    }

    fun insertMovie(movieList: ArrayList<Movie>) {
        movieDao.insertMovie(movieList)
    }

    private fun getMovie(page: Int, category: String): MovieResponse {
        return try {
            val movieList = movieDao.getMovie(category, PAGE_SIZE, ((page-1)*PAGE_SIZE))
            MovieResponse(ResponseStatus.RESPONSE_SUCCESS, "Movie get successfully", page, arrayListOf<Movie>().apply { addAll(movieList) }, null)
        } catch (exception: Exception) {
            MovieResponse(ResponseStatus.RESPONSE_FAILED, "Something went wrong", page, null, null)
        }
    }
}