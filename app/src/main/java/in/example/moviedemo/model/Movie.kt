package `in`.example.moviedemo.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Movie(
        @SerializedName("id")
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        val id: Long,
        @SerializedName("title")
        @ColumnInfo(name = "title")
        val title: String,
        @SerializedName("poster_path")
        @ColumnInfo(name = "poster_path")
        val posterPath: String,
        @SerializedName("release_date")
        @ColumnInfo(name = "release_date")
        val releaseDate: String,
        @SerializedName("vote_average")
        @ColumnInfo(name = "vote_average")
        val voteAverage: Double,
        /*@SerializedName("genre_ids")
        @ColumnInfo(name = "genre_ids")
        val genreIds: ArrayList<Int>,*/
        @SerializedName("category")
        @ColumnInfo(name = "category")
        var category: String?
)