package `in`.example.moviedemo.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import `in`.example.moviedemo.R
import `in`.example.moviedemo.utils.EndlessScrollListener
import `in`.example.moviedemo.utils.LoadingStatus
import `in`.example.moviedemo.utils.ViewType
import `in`.example.moviedemo.databinding.FragmentMovieBinding
import `in`.example.moviedemo.model.Movie
import `in`.example.moviedemo.viewmodel.MovieViewModel
import `in`.ms.moviedemo.adapter.MovieListAdapter
import android.content.Context
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieFragment : Fragment(), LifecycleObserver {

    private lateinit var binding: FragmentMovieBinding
    private val _context: Context by lazy {
        requireActivity()
    }
    private val gridLayoutManager: GridLayoutManager by lazy {
        GridLayoutManager(_context, 1)
    }

    private lateinit var movieList: ArrayList<Movie>

    private lateinit var adapter: MovieListAdapter

    private var isLoadMore: Boolean = true

    private val viewModel: MovieViewModel by viewModels()

    private var isSearchApplied = false

    private lateinit var movieType: String

    companion object {
        fun getInstance(movieType: String) : MovieFragment {
            return MovieFragment().apply {
                arguments = Bundle().apply {
                    putString("movie_type", movieType)
                }
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreated() {
        setHasOptionsMenu(true)
        activity?.lifecycle?.removeObserver(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        movieType = arguments?.getString("movie_type", "popular") ?: "popular"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_movie, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.movieRecyclerView.setHasFixedSize(true)
        binding.movieRecyclerView.layoutManager = gridLayoutManager

        binding.movieRecyclerView.addOnScrollListener(object : EndlessScrollListener(gridLayoutManager) {
            override fun onLoadMore(pageIndex: Int, totalItemsCount: Int, view: RecyclerView?) {
                if (isLoadMore && !isSearchApplied) {
                    getMovies()
                }
            }
        })

        movieList = arrayListOf()
        adapter = MovieListAdapter(_context, LayoutInflater.from(_context), movieList)
        binding.movieRecyclerView.adapter = adapter

        /*mainViewModel.viewType.observe(requireActivity(), Observer {
            gridLayoutManager.spanCount = if (it == ViewType.TYPE_GRID) 2 else 1
            adapter.setViewType(it)
            adapter.notifyDataSetChanged()
        })*/

        observeUI()
        getMovies()

    }

    private fun observeUI() {
        if (movieType == "popular") {
            viewModel.popularMovieList.observe(viewLifecycleOwner) {
                movieList.clear()
                movieList.addAll(it)
                adapter.notifyDataSetChanged()
            }
        } else if (movieType == "top_rated") {
            viewModel.topRatedMovieList.observe(viewLifecycleOwner) {
                movieList.clear()
                movieList.addAll(it)
                adapter.notifyDataSetChanged()
            }
        } else {
            viewModel.upcomingMovieList.observe(viewLifecycleOwner) {
                movieList.clear()
                movieList.addAll(it)
                adapter.notifyDataSetChanged()
            }
        }

        viewModel.loadingStatus.observe(viewLifecycleOwner) {
            binding.movieProgressBar.visibility = if (it == LoadingStatus.STATUS_LOADING) VISIBLE else GONE
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.lifecycle?.addObserver(this)
    }

    override fun onResume() {
        super.onResume()
        activity?.invalidateOptionsMenu()
    }

    private fun getMovies() {
        if (movieType == "popular")
            viewModel.getPopularMovie()
        else if (movieType == "top_rated")
            viewModel.getTopRatedMovie()
        else
            viewModel.getUpcomingMovie()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_movie, menu)

        val searchView = menu.findItem(R.id.menu_movie_search).actionView as SearchView
        searchView.maxWidth = Int.MAX_VALUE

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                //isSearchApplied = true
                adapter.filter.filter(newText)
                return false
            }

        })

        searchView.setOnCloseListener {
            //isSearchApplied = false
            adapter.filter.filter("")
            false
        }

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_movie_layout -> {
                if (adapter.getViewType() == ViewType.TYPE_LIST) {
                    gridLayoutManager.spanCount = 2
                    item.setIcon(R.drawable.ic_grid_view)
                    adapter.setViewType(ViewType.TYPE_GRID)
                } else {
                    gridLayoutManager.spanCount = 1
                    item.setIcon(R.drawable.ic_list)
                    adapter.setViewType(ViewType.TYPE_LIST)
                }
                val visibleItemPos = gridLayoutManager.findFirstVisibleItemPosition()
                binding.movieRecyclerView.adapter = adapter
                binding.movieRecyclerView.scrollToPosition(visibleItemPos)
            }
        }
        return true
    }
}