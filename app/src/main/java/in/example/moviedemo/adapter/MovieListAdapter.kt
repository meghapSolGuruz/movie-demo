package `in`.ms.moviedemo.adapter

import `in`.example.moviedemo.App
import `in`.example.moviedemo.R
import `in`.example.moviedemo.activity.MovieDetailActivity
import `in`.example.moviedemo.utils.ViewType
import `in`.example.moviedemo.databinding.RowMovieGridBinding
import `in`.example.moviedemo.databinding.RowMovieListBinding
import `in`.example.moviedemo.model.Movie
import `in`.example.moviedemo.utils.NetworkUtils
import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MovieListAdapter(private val _context: Context, private val layoutInflater: LayoutInflater, private val movieList: ArrayList<Movie>) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable {

    private var tmpMovieList = movieList

    private var viewType: ViewType = ViewType.TYPE_LIST

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return  if (this.viewType == ViewType.TYPE_GRID) {
            GridViewHolder(DataBindingUtil.inflate(layoutInflater, R.layout.row_movie_grid, parent, false))
        } else {
            ListViewHolder(DataBindingUtil.inflate(layoutInflater, R.layout.row_movie_list, parent, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is GridViewHolder) {
            holder.binding.movie = tmpMovieList[position]

            if (tmpMovieList[position].releaseDate.isNotEmpty()) {
                val originalFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
                val targetFormat: DateFormat = SimpleDateFormat("dd MMM, yyyy", Locale.ENGLISH)
                holder.binding.date = targetFormat.format(originalFormat.parse(tmpMovieList[position].releaseDate) as Date)
            }

        } else if (holder is ListViewHolder) {
            holder.binding.movie = tmpMovieList[position]
            if (tmpMovieList[position].releaseDate.isNotEmpty()) {
                val originalFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
                val targetFormat: DateFormat = SimpleDateFormat("dd MMM, yyyy", Locale.ENGLISH)
                holder.binding.date = targetFormat.format(originalFormat.parse(tmpMovieList[position].releaseDate) as Date)
            }
        }
    }

    override fun getItemCount(): Int {
        return tmpMovieList.size
    }

    fun setViewType(viewType: ViewType) {
        this.viewType = viewType
        Log.e("ViewType", this.viewType.name)
    }

    fun getViewType() = viewType

    inner class GridViewHolder(val binding: RowMovieGridBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener {
                if (NetworkUtils.isInternetConnected()) {
                    val intent = Intent(_context, MovieDetailActivity::class.java)
                    intent.putExtra("movie_id", movieList[adapterPosition].id)
                    _context.startActivity(intent)
                } else {
                    Toast.makeText(App.getAppContext(), "Internet connection not available", Toast.LENGTH_LONG).show()
                }
            }
        }
    }
    inner class ListViewHolder(val binding: RowMovieListBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener {
                val intent = Intent(_context, MovieDetailActivity::class.java)
                intent.putExtra("movie_id",  movieList[adapterPosition].id)
                _context.startActivity(intent)
            }
        }
    }


    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                if (!TextUtils.isEmpty(constraint)) {
                    //tmpMovieList.clear()
                    //tmpMovieList.addAll(movieList)
                    tmpMovieList = movieList.filter {
                        it.title.toLowerCase(Locale.getDefault()).contains(constraint.toString().toLowerCase(Locale.getDefault()))
                    } as ArrayList<Movie>
                    Log.e("Adapter", tmpMovieList.toString())
                } else {
                    //tmpMovieList.clear()
                    //tmpMovieList.addAll(movieList)
                    tmpMovieList = movieList
                }

                return FilterResults().apply {
                    values = tmpMovieList
                }
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                notifyDataSetChanged()
            }
        }
    }

}