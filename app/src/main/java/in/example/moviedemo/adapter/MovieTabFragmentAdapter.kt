package `in`.example.moviedemo.adapter

import `in`.example.moviedemo.fragment.MovieFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class MovieTabFragmentAdapter(fragmentActivity: FragmentActivity, private val fragmentList: ArrayList<MovieFragment>) : FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount(): Int {
        return fragmentList.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragmentList[position] /*when(position) {
            0 -> Fragment()
            1 -> Fragment()
            else -> Fragment()
        }*/
    }

}